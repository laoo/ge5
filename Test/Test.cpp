#include "StructDecl.hpp"
#include "StructDef.hpp"
#include "Parser.hpp"
#include <boost/locale.hpp>



int wmain( int argc, wchar_t* argv[] )
{
  ge5::ParserData data
  {
    { nsName::characterSetTable.data(), nsName::characterSetTable.size() },
    { nsName::symbolTable.data(), nsName::symbolTable.size() },
    { nsName::groupTable.data(), nsName::groupTable.size() },
    { nsName::productionTable.data(), nsName::productionTable.size() },
    { nsName::dfaTable.data(), nsName::dfaTable.size() },
    { nsName::lalrTable.data(), nsName::lalrTable.size() },
    nsName::initialDFAState, nsName::initialLALRState,
    &nsName::dispatch
  };

  ge5::Parser p{ data };

  auto result = p.parse( u"display 5 + 3 * - 4 display(5+3)*-4" );


  return 0;
}

namespace nsName
{

struct NumberNode : public ge5::IParseTreeNode
{
  virtual ~NumberNode() = default;

  NumberNode( std::u16string_view const& text ) : number{ std::stoi( boost::locale::conv::utf_to_utf<char>( text.data(), text.data() + text.size() ) ) } //utf8
  {
  }

  int number;
};

  // <Statements> ::= <Statement> <Statements>
NStatements reduce( NStatement n0, NStatements n1 )
{
  return {};
}

// <Statements> ::= <Statement>
NStatements reduce( NStatement n0 )
{
  return {};
}
// <Statement> ::= display <Expression>
NStatement reduce( Tdisplay t0, NExpression n1 )
{
  std::cout << dynamic_cast<NumberNode &>( *n1 ).number << std::endl;
  return {};
}
// <Expression> ::= <Expression> > <Add Exp>
NExpression reduce( NExpression n0, Tu003e t1, NAddu0020Exp n2 )
{
  auto & l = dynamic_cast<NumberNode &>( *n0 );
  auto & r = dynamic_cast<NumberNode &>( *n2 );
  l.number = l.number > r.number ? 1 : 0;
  
  return n0;
}
// <Expression> ::= <Expression> < <Add Exp>
NExpression reduce( NExpression n0, Tu003c t1, NAddu0020Exp n2 )
{
  auto & l = dynamic_cast<NumberNode &>( *n0 );
  auto & r = dynamic_cast<NumberNode &>( *n2 );
  l.number = l.number < r.number ? 1 : 0;

  return n0;
}
// <Expression> ::= <Expression> <= <Add Exp>
NExpression reduce( NExpression n0, Tu003cu003d t1, NAddu0020Exp n2 )
{
  auto & l = dynamic_cast<NumberNode &>( *n0 );
  auto & r = dynamic_cast<NumberNode &>( *n2 );
  l.number = l.number <= r.number ? 1 : 0;

  return n0;
}
// <Expression> ::= <Expression> >= <Add Exp>
NExpression reduce( NExpression n0, Tu003eu003d t1, NAddu0020Exp n2 )
{
  auto & l = dynamic_cast<NumberNode &>( *n0 );
  auto & r = dynamic_cast<NumberNode &>( *n2 );
  l.number = l.number >= r.number ? 1 : 0;

  return n0;
}
// <Expression> ::= <Expression> == <Add Exp>
NExpression reduce( NExpression n0, Tu003du003d t1, NAddu0020Exp n2 )
{
  auto & l = dynamic_cast<NumberNode &>( *n0 );
  auto & r = dynamic_cast<NumberNode &>( *n2 );
  l.number = l.number == r.number ? 1 : 0;

  return n0;
}
// <Expression> ::= <Expression> <> <Add Exp>
NExpression reduce( NExpression n0, Tu003cu003e t1, NAddu0020Exp n2 )
{
  auto & l = dynamic_cast<NumberNode &>( *n0 );
  auto & r = dynamic_cast<NumberNode &>( *n2 );
  l.number = l.number != r.number ? 1 : 0;

  return n0;
}
// <Expression> ::= <Add Exp>
NExpression reduce( NAddu0020Exp n0 )
{
  return { std::move( n0 ) };
}
// <Add Exp> ::= <Add Exp> + <Mult Exp>
NAddu0020Exp reduce( NAddu0020Exp n0, Tu002b t1, NMultu0020Exp n2 )
{
  auto & l = dynamic_cast<NumberNode &>( *n0 );
  auto & r = dynamic_cast<NumberNode &>( *n2 );
  l.number = l.number + r.number;

  return n0;
}
// <Add Exp> ::= <Add Exp> - <Mult Exp>
NAddu0020Exp reduce( NAddu0020Exp n0, Tu002d t1, NMultu0020Exp n2 )
{
  auto & l = dynamic_cast<NumberNode &>( *n0 );
  auto & r = dynamic_cast<NumberNode &>( *n2 );
  l.number = l.number - r.number;

  return n0;
}
// <Add Exp> ::= <Mult Exp>
NAddu0020Exp reduce( NMultu0020Exp n0 )
{
  return { std::move( n0 ) };
}
// <Mult Exp> ::= <Mult Exp> * <Negate Exp>
NMultu0020Exp reduce( NMultu0020Exp n0, Tu002a t1, NNegateu0020Exp n2 )
{
  auto & l = dynamic_cast<NumberNode &>( *n0 );
  auto & r = dynamic_cast<NumberNode &>( *n2 );
  l.number = l.number * r.number;

  return n0;
}
// <Mult Exp> ::= <Mult Exp> / <Negate Exp>
NMultu0020Exp reduce( NMultu0020Exp n0, Tu002f t1, NNegateu0020Exp n2 )
{
  auto & l = dynamic_cast<NumberNode &>( *n0 );
  auto & r = dynamic_cast<NumberNode &>( *n2 );
  l.number = l.number / r.number;

  return n0;
}
// <Mult Exp> ::= <Negate Exp>
NMultu0020Exp reduce( NNegateu0020Exp n0 )
{
  return { std::move( n0 ) };
}
// <Negate Exp> ::= - <Value>
NNegateu0020Exp reduce( Tu002d t0, NValue n1 )
{
  auto & p = dynamic_cast<NumberNode &>( *n1 );
  p.number = -p.number;
  return { std::move( n1 ) };
}
// <Negate Exp> ::= <Value>
NNegateu0020Exp reduce( NValue n0 )
{
  return { std::move( n0 ) };
}
// <Value> ::= NumberLiteral
NValue reduce( TNumberLiteral t0 )
{
  return { std::make_unique<NumberNode>( t0 ) };
}
// <Value> ::= ( <Expression> )
NValue reduce( Tu0028 t0, NExpression n1, Tu0029 t2 )
{
  return { std::move( n1 ) };
}

}
