#ifndef GE5_POSITION_HPP
#define GE5_POSITION_HPP

#include <sstream>

#include "ISourceProxy.hpp"

namespace GE5
{

struct Position
{
  Position( std::shared_ptr<ISourceProxy> proxy, int l = -1, int c = -1 ) :
    sourceProxy{ std::move( proxy ) },
    lineBegin{ l }, columnBegin{ c }, lineEnd{ l }, columnEnd{ c } {}

  std::shared_ptr<ISourceProxy> sourceProxy;
  int lineBegin;
  int columnBegin;
  int lineEnd;
  int columnEnd;

  std::wstring wstring() const
  {
    std::wstringstream ss;
    ss << sourceProxy->getName();
    ss << L"(" << lineBegin << L":" << columnBegin;
    if ( lineEnd != lineBegin )
    {
      ss << L" - " << lineEnd << L":";
      ss << ( columnEnd - 1 ) << L")";
    }
    else
    {
      if ( columnBegin < columnEnd - 1 )
      {
        ss << L"-";
        ss << ( columnEnd - 1 );
      }
      ss << L")";
    }

    return ss.str();
  }

  std::wstring range() const
  {
    std::wstringstream ss;
    ss << L"(" << lineBegin << L":" << columnBegin;
    if ( lineEnd != lineBegin )
    {
      ss << L" - " << lineEnd << L":";
      ss << ( columnEnd - 1 ) << L")";
    }
    else
    {
      if ( columnBegin < columnEnd - 1 )
      {
        ss << L"-";
        ss << ( columnEnd - 1 );
      }
      ss << L")";
    }

    return ss.str();
  }
};


} //namespace GE5

#endif //GE5_POSITION_HPP
