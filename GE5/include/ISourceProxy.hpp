#ifndef GE5_ISOURCEPROXY_HPP
#define GE5_ISOURCEPROXY_HPP

#include <string>

namespace GE5
{

class ISourceProxy
{
public:
  virtual ~ISourceProxy() = default;

  virtual std::wstring const& getName() const = 0;

  // Returning empty lines by default.
  virtual std::wstring getLine( int number ) const
  {
    return std::wstring();
  }

  virtual int lines() const
  {
    return 0;
  }

};

} //namespace GE5

#endif //GE5_ISOURCEPROXY_HPP
