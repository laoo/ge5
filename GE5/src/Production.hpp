#ifndef GE5_PRODUCTION_HPP
#define GE5_PRODUCTION_HPP


#include "IParserServer.hpp"

namespace GE5
{
struct Production
{
public:
  Production();
  Production( int nonterminal, std::vector<int> symbolIndices );

  void setName( std::u16string name );
  std::u16string getName() const;
  void setReductor( Reductor reductor );
  int nonterminal() const;
  Reductor const& getReductor() const;

  std::vector<int> const& symbolIndices() const;

  int mNonterminal;
  std::vector<int> mSymbolIndices;
  std::u16string mName;
  Reductor mReductor;
};

}

#endif //GE5_PRODUCTION_HPP
