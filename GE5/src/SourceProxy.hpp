#ifndef GE5_SOURCEPROXY_HPP
#define GE5_SOURCEPROXY_HPP

#include "ISourceProxy.hpp"

namespace GE5
{

class SourceProxy : public ISourceProxy
{
public:

  SourceProxy( std::wstring name );
  ~SourceProxy() override = default;

  std::wstring const& getName() const override;

private:
  std::wstring const mName;

};

}

#endif //GE5_SOURCEPROXY_HPP
