#include "stdafx.h"
#include "LALRState.hpp"

namespace GE5
{

LALRState::LALRState()
{
}

LALRState::LALRState( std::vector< Action > action ) : mAction{ std::move( action ) }
{
}

LALRState::Action const* LALRState::action( int symbolIdx ) const
{
  auto it = std::find_if( mAction.cbegin(), mAction.cend(), [symbolIdx]( Action const& a )
  {
    return a.symbolIndex == symbolIdx;
  } );

  if ( it != mAction.cend() )
  {
    return &*it;
  }
  else
  {
    return nullptr;
  }
}


}

