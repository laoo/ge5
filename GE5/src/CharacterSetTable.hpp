#ifndef GE5_CHARACTERSETTABLE_HPP
#define GE5_CHARACTERSETTABLE_HPP

#include <ostream>

namespace GE5
{

struct CharacterSetTable
{
public:

  struct CharacterSetRange
  {
    CharacterSetRange( char16_t aStart, char16_t aEnd ) : start( aStart ), end( aEnd ) {}

    char16_t start;
    char16_t end;

  };

  CharacterSetTable();
  CharacterSetTable( std::vector<CharacterSetRange> && ranges );

  bool contains( char16_t c ) const;
  size_t size() const;


  std::vector<CharacterSetRange> mRanges;

};

}

#endif //GE5_CHARACTERSETTABLE_HPP
