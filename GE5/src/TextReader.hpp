#ifndef GE5_TEXTREADER_HPP
#define GE5_TEXTREADER_HPP

namespace GE5
{

class TextReader
{
public:
  TextReader( std::wstring source );
  virtual ~TextReader() = default;


  static std::unique_ptr<TextReader> create( std::string text );
  static std::unique_ptr<TextReader> create( std::u16string text );
  static std::unique_ptr<TextReader> create( char const* aBegin, char const* aEnd );
  static std::unique_ptr<TextReader> create( char16_t const* aBegin, char16_t const* aEnd );
  static std::unique_ptr<TextReader> create( std::wstring source, std::istream & istrm );

  virtual bool read( char16_t & c ) = 0;

  std::wstring const& source() const
  {
    return mSource;
  }

protected:
  std::wstring const mSource;
};

class StringTextReader : public TextReader
{
public:
  StringTextReader( std::string text );
  ~StringTextReader() override = default;

  bool read( char16_t & c ) override;

private:
  std::string mText;
  size_t mOffset;
};

class U16StringTextReader : public TextReader
{
public:
  U16StringTextReader( std::u16string text );
  ~U16StringTextReader() override = default;

  bool read( char16_t & c ) override;

private:
  std::u16string mText;
  size_t mOffset;
};

class CharTextReader : public TextReader
{
public:
  CharTextReader( char const* aBegin, char const* aEnd );
  ~CharTextReader() override = default;

  bool read( char16_t & c ) override;

private:
  char const* mBegin;
  char const* mEnd;
};

class Char16TextReader : public TextReader
{
public:
  Char16TextReader( char16_t const* aBegin, char16_t const* aEnd );
  ~Char16TextReader() override = default;

  bool read( char16_t & c ) override;

private:
  char16_t const* mBegin;
  char16_t const* mEnd;
};

class StreamReader : public TextReader
{
public:
  StreamReader( std::wstring source, std::istream & istrm );
  ~StreamReader() override = default;

  bool read( char16_t & c ) override;

private:
  std::istream & mIstrm;
};

}

#endif //GE5_TEXTREADER_HPP
