#ifndef GE5_PARSER_HPP
#define GE5_PARSER_HPP

#include "ParserServer.hpp"
#include "ParserData.hpp"

#include "Token.hpp"

namespace GE5
{
class TextReader;
class ISourceProxy;


class Parser : public ParserData, public IParser
{
public:

  ~Parser() override = default;
  Parser( Parser && other ) = default;


  Result parse() override;

private:

  enum LALRResult
  {
    LALRAccept,
    LALRShift,
    LALRReduceNormal
  };

  friend class ParserServer;

  Parser( std::shared_ptr<ParserServer> aServer, std::unique_ptr<TextReader> aTextReader, int id, bool rememberText );
  Parser( Parser const& ) = default;

  char16_t lookahead( size_t aIdx );
  std::u16string Parser::lookaheadString( size_t size ) const;

  Token produceToken();
  Token lookaheadDFA();
  LALRResult parseLALR( Token & token );
  void comsumeBuffer( size_t aCnt );
  Symbol const& Parser::findKind( Symbol::Type type ) const;

private:
  std::unique_ptr<TextReader> mTextReader;
  std::shared_ptr<ISourceProxy> mSourceProxy;
  bool mRememberText;

  int mCurrentLALR;

  int mInternalLine;
  int mInternalColumn;

  std::vector<Token> mStack;
  std::vector<Token> mGroupStack;
  std::vector<char16_t> mLookaheadBuffer;

  int mId;

  static const char16_t EOFChar = 0xdb00;
};

}

#endif //GE5_PARSER_HPP
