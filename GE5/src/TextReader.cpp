#include "stdafx.h"
#include "TextReader.hpp"

namespace GE5
{

TextReader::TextReader( std::wstring source ) : mSource{ std::move( source ) }
{
}

std::unique_ptr<TextReader> TextReader::create( std::string text )
{
  return std::unique_ptr<TextReader>( new StringTextReader{ std::move( text ) } );
}

std::unique_ptr<TextReader> TextReader::create( std::u16string text )
{
  return std::unique_ptr<TextReader>( new U16StringTextReader{ std::move( text ) } );
}

std::unique_ptr<TextReader> TextReader::create( char const* aBegin, char const* aEnd )
{
  return std::unique_ptr<TextReader>( new CharTextReader{ aBegin, aEnd } );
}

std::unique_ptr<TextReader> TextReader::create( char16_t const* aBegin, char16_t const* aEnd )
{
  return std::unique_ptr<TextReader>( new Char16TextReader{ aBegin, aEnd } );
}

std::unique_ptr<TextReader> TextReader::create( std::wstring source, std::istream & istrm )
{
  return std::unique_ptr<TextReader>( new StreamReader{ std::move( source ), istrm } );
}

StringTextReader::StringTextReader( std::string text ) : TextReader{ L"memory" }, mText{ std::move( text ) }, mOffset{ 0 }
{
}

bool StringTextReader::read( char16_t & c )
{
  if ( mOffset < mText.size() )
  {
    c = static_cast<unsigned char>( mText[ mOffset++ ] );
    return true;
  }
  else
  {
    return false;
  }
}


U16StringTextReader::U16StringTextReader( std::u16string text ) : TextReader{ L"memory" }, mText{ std::move( text ) }, mOffset{ 0 }
{
}

bool U16StringTextReader::read( char16_t & c )
{
  if ( mOffset < mText.size() )
  {
    c = mText[ mOffset++ ];
    return true;
  }
  else
  {
    return false;
  }
}

CharTextReader::CharTextReader( char const* aBegin, char const* aEnd ) : TextReader{ L"memory" }, mBegin{ aBegin }, mEnd{ aEnd }
{
}

bool CharTextReader::read( char16_t & c )
{
  if ( mBegin < mEnd )
  {
    c = static_cast<unsigned char>( *mBegin++ );
    return true;
  }
  else
  {
    return false;
  }
}

Char16TextReader::Char16TextReader( char16_t const* aBegin, char16_t const* aEnd ) : TextReader( L"memory" ), mBegin( aBegin ), mEnd( aEnd )
{
}

bool Char16TextReader::read( char16_t & c )
{
  if ( mBegin < mEnd )
  {
    c = *mBegin++;
    return true;
  }
  else
  {
    return false;
  }
}


StreamReader::StreamReader( std::wstring source, std::istream & istrm ) : TextReader{ std::move( source ) }, mIstrm{ istrm }
{
}

bool StreamReader::read( char16_t & c )
{
  char ch;
  mIstrm.get( ch );
  if ( mIstrm.good() )
  {
    c = ch;
    return true;
  }
  else
  {
    return false;
  }
}


}

