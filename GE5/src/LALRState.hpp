#ifndef GE5_LALRSTATE_HPP
#define GE5_LALRSTATE_HPP

namespace GE5
{

struct LALRState
{
public:

  struct Action
  {
    enum Type
    {
      Shift = 1,
      Reduce = 2,
      Goto = 3,
      Accept = 4
    };

    Action( int aSymbolIndex, Type aAction, int aTargetIndex ) : symbolIndex{ aSymbolIndex }, action{ aAction }, targetIndex{ aTargetIndex } {}

    int symbolIndex;
    Type action;
    int targetIndex;
  };


  LALRState();
  LALRState( std::vector< Action > action );

  Action const* action( int symbolIdx ) const;

  std::vector< Action > mAction;
};

}

#endif //GE5_LALRSTATE_HPP
