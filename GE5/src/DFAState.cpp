#include "stdafx.h"
#include "DFAState.hpp"

namespace GE5
{

DFAState::DFAState()
{
}

DFAState::DFAState( std::optional<int> terminalIndex, std::vector< DFAState::Edge > edges ) :
  mTerminalIndex{ std::move( terminalIndex ) }, mEdges{ std::move( edges ) }
{
}

std::vector< DFAState::Edge >::const_iterator DFAState::cbegin() const
{
  return mEdges.cbegin();
}

std::vector< DFAState::Edge >::const_iterator DFAState::cend() const
{
  return mEdges.cend();
}

std::optional<int> const& DFAState::terminalIndex() const
{
  return mTerminalIndex;
}

}

