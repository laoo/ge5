#include "stdafx.h"
#include "CharacterSetTable.hpp"

namespace GE5
{

CharacterSetTable::CharacterSetTable() : mRanges{}
{
}

CharacterSetTable::CharacterSetTable( std::vector<CharacterSetRange> && ranges ) : mRanges{ std::move( ranges ) }
{
}

bool CharacterSetTable::contains( char16_t c ) const
{
  return std::any_of( mRanges.cbegin(), mRanges.cend(), [c]( CharacterSetRange const& r )
  {
    return c >= r.start && c <= r.end;
  } );
}

size_t CharacterSetTable::size() const
{
  return mRanges.size();
}

}

