#include "stdafx.h"
#include "TextSourceProxy.hpp"

namespace GE5
{

TextSourceProxy::TextSourceProxy( std::wstring name ) : SourceProxy{ std::move( name ) }
{
  mLinesOffsets.push_back( 0 );
}

void TextSourceProxy::addChar( char16_t c )
{
  mData.push_back( c );
}

void TextSourceProxy::markEol()
{
  mLinesOffsets.push_back( mData.size() );
}

std::wstring TextSourceProxy::getLine( int number ) const
{
  if ( number >= 1 && ( size_t )number < mLinesOffsets.size() )
  {
    size_t beg = mLinesOffsets[( size_t )number - 1];
    size_t end = ( size_t )number - 1 < mLinesOffsets.size() - 1 ? mLinesOffsets[( size_t )number] : mData.size();

    return boost::locale::conv::utf_to_utf<wchar_t>( &mData[0] + beg, &mData[0] + end );
  }
  else
  {
    return {};
  }
}

int TextSourceProxy::lines() const
{
  return ( int )mLinesOffsets.size();
}

}

