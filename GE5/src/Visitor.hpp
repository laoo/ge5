#ifndef GE5_VISITOR_HPP
#define GE5_VISITOR_HPP

#include "CharacterSetTable.hpp"
#include "Symbol.hpp"
#include "Group.hpp"
#include "DFAState.hpp"
#include "LALRState.hpp"
#include "Production.hpp"

namespace GE5
{

class Visitor
{
public:
  virtual ~Visitor() {}

  virtual void addProperty( int index, std::u16string name, std::u16string value ) = 0;
  virtual void setTableCounts( int symbolTable, int characterSetTable, int ruleTable, int dfaTable, int lalrTable, int groupTable ) = 0;
  virtual void setInitialState( int dfa, int lalr ) = 0;
  virtual void setCharacterSetTable( int index, int unicodePlane, std::vector< CharacterSetTable::CharacterSetRange > ranges ) = 0;
  virtual void addSymbol( int index, std::u16string name, Symbol::Type type ) = 0;
  virtual void addGroup( int index, std::u16string name, int containerIndex, int startIndex, int endIndex, Group::AdvanceMode advanceMode, Group::EndingMode endingMode, std::vector<int> nesting ) = 0;
  virtual void addProduction( int index, int nonterminal, std::vector<int> symbolIndices ) = 0;
  virtual void addDFAState( int index, std::optional<int> terminalIndex, std::vector< DFAState::Edge > edges ) = 0;
  virtual void addLALRState( int index, std::vector< LALRState::Action > actions ) = 0;

};

}

#endif //GE5_VISITOR_HPP
