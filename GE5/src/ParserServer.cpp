#include "stdafx.h"
#include "Exception.hpp"
#include "ParserServer.hpp"
#include "Parser.hpp"
#include "Record.hpp"
#include "EGTReader.hpp"
#include "TextReader.hpp"

#pragma warning( push )
#pragma warning( disable : 4566 )
//#include "g:\out.hpp"
#pragma warning( pop )

namespace GE5
{

std::shared_ptr<IParserServer> createServer( std::filesystem::path const& aTableFileName )
{
  try
  {
    std::ifstream fin{ aTableFileName, std::ios::binary };

    if ( fin.good() )
    {
      return createServer( fin );
    }
    else
      throw EGTException{ "Can't open EGT file" };
  }
  catch ( EGTException const& e )
  {
    std::cerr << e.what();
    return {};
  }
}

std::shared_ptr<IParserServer> createServer( char const* aBegin, char const* aEnd )
{
  return std::make_shared<ParserServer>( aBegin, aEnd );
}


std::shared_ptr<IParserServer> createServer( std::ifstream & fin )
{
  fin.seekg( 0, std::ios::end );
  size_t length = ( size_t )fin.tellg();
  fin.seekg( 0, std::ios::beg );

  std::vector<char> buf( length );
  fin.read( &buf[0], length );
  return createServer( buf );
}

std::shared_ptr<IParserServer> createServer( std::vector<char> const& tables )
{
  return std::make_shared<ParserServer>( &tables.front(), &tables.front() + tables.size() );
}

ParserServer::ParserServer( char const* aBegin, char const* aEnd ) : mDFAInitialState{}, mLALRInitialState{}, mReductorsValidated{}
{
  char const* next = readHeader( aBegin );

  EGTReader reader{ next, aEnd };

  for ( ;; )
  {
    Record record = reader.read();
    if ( !record.visit( *this ) )
      break;
  }

  for ( auto & p : mProductionTable )
  {
    std::basic_stringstream<char16_t, std::char_traits<char16_t>, std::allocator<char16_t> > ss;
    ss << '<' << mSymbolTable[p.nonterminal()].name() << '>' << ' ' << ':' << ':' << '=';

    if ( p.symbolIndices().empty() )
    {
      ss << ' ' << '<' << '>';
    }
    else
    {
      for ( auto it = p.symbolIndices().cbegin(); it != p.symbolIndices().cend() && ( ss << ' ', true ); ++it )
      {
        Symbol const& s = mSymbolTable[*it];
        if ( s.type() == Symbol::Nonterminal )
        {
          ss << '<' << s.name() << '>';
        }
        else if ( s.type() == Symbol::Content )
        {
          ss << s.name();
        }
        else
        {
          assert( false );
        }
      }
    }
    p.setName( ss.str() );
  }

}

char const* ParserServer::readHeader( char const* aHeader ) const
{
#ifdef WIN32
  static wchar_t const header[] = L"GOLD Parser Tables/v5.0";
  typedef wchar_t const wtype;
#else
  static char16_t const header[] = u"GOLD Parser Tables/v5.0";
  typedef char16_t const wtype;
#endif

  bool eq = std::equal( header, header + sizeof( header ) / sizeof( wtype ), reinterpret_cast< wtype* >( aHeader ) );

  if ( !eq )
    throw EGTException{ "GOLD Parser Tables/v5.0" };

  return aHeader + sizeof header;
}

std::unique_ptr<IParser> ParserServer::open( char const* aBegin, char const* aEnd, int id, bool rememberText )
{
  std::u16string invalidList;
  if ( !validateReductors( invalidList ) )
    throw InsufficientReductorsException{ boost::locale::conv::from_utf( boost::locale::conv::utf_to_utf<wchar_t>( invalidList ), "UTF-8" ) };

  return std::unique_ptr<IParser>( new Parser{ shared_from_this(), TextReader::create( aBegin, aEnd ), id, rememberText } );
}

std::unique_ptr<IParser> ParserServer::open( char16_t const* aBegin, char16_t const* aEnd, int id, bool rememberText )
{
  std::u16string invalidList;
  if ( !validateReductors( invalidList ) )
    throw InsufficientReductorsException{ boost::locale::conv::from_utf( boost::locale::conv::utf_to_utf<wchar_t>( invalidList ), "UTF-8" ) };

  return std::unique_ptr<IParser>( new Parser{ shared_from_this(), TextReader::create( aBegin, aEnd ), id, rememberText } );
}

std::unique_ptr<IParser> ParserServer::open( std::string text, int id, bool rememberText )
{
  std::u16string invalidList;
  if ( !validateReductors( invalidList ) )
    throw InsufficientReductorsException{ boost::locale::conv::from_utf( boost::locale::conv::utf_to_utf<wchar_t>( invalidList ), "UTF-8" ) };

  return std::unique_ptr<IParser>( new Parser{ shared_from_this(), TextReader::create( std::move( text ) ), id, rememberText } );
}

std::unique_ptr<IParser> ParserServer::open( std::u16string text, int id, bool rememberText )
{
  std::u16string invalidList;
  if ( !validateReductors( invalidList ) )
    throw InsufficientReductorsException{ boost::locale::conv::from_utf( boost::locale::conv::utf_to_utf<wchar_t>( invalidList ), "UTF-8" ) };

  return std::unique_ptr<IParser>( new Parser{ shared_from_this(), TextReader::create( std::move( text ) ), id, rememberText } );
}

std::unique_ptr<IParser> ParserServer::open( std::wstring source, std::istream & istrm, int id, bool rememberText )
{
  std::u16string invalidList;
  if ( !validateReductors( invalidList ) )
    throw InsufficientReductorsException{ boost::locale::conv::from_utf( boost::locale::conv::utf_to_utf<wchar_t>( invalidList ), "UTF-8" ) };

  return std::unique_ptr<IParser>( new Parser{ shared_from_this(), TextReader::create( std::move( source ), istrm ), id, rememberText } );
}

void ParserServer::addProperty( int index, std::u16string name, std::u16string value )
{
  mProperties.insert( std::make_pair( std::move( name ), std::move( value ) ) );
}

void ParserServer::setTableCounts( int symbolTable, int characterSetTable, int ruleTable, int dfaTable, int lalrTable, int groupTable )
{
  mSymbolTable.resize( symbolTable );
  mCharacterSetTable.resize( characterSetTable );
  mProductionTable.resize( ruleTable );
  mDFATable.resize( dfaTable );
  mLALRTable.resize( lalrTable );
  mGroupTable.resize( groupTable );
}

void ParserServer::setInitialState( int dfa, int lalr )
{
  mDFAInitialState = dfa;
  mLALRInitialState = lalr;

}

void ParserServer::setCharacterSetTable( int index, int unicodePlane, std::vector< CharacterSetTable::CharacterSetRange > ranges )
{
  mCharacterSetTable[index] = CharacterSetTable{ std::move( ranges ) };
}

void ParserServer::addSymbol( int index, std::u16string name, Symbol::Type type )
{
  mSymbolTable[index] = Symbol{ std::move( name ), type, (size_t)index };
}

void ParserServer::addGroup( int index, std::u16string name, int containerIndex, int startIndex, int endIndex, Group::AdvanceMode advanceMode, Group::EndingMode endingMode, std::vector<int> nesting )
{
  mGroupTable[index] = Group{ std::move( name ), containerIndex, startIndex, endIndex, advanceMode, endingMode, std::move( nesting ) };

  mSymbolTable[containerIndex].setGroup( index );
  mSymbolTable[startIndex].setGroup( index );
  mSymbolTable[endIndex].setGroup( index );
}

void ParserServer::addProduction( int index, int nonterminal, std::vector<int> symbolIndices )
{
  mProductionTable[index] = Production{ nonterminal, std::move( symbolIndices ) };
}

void ParserServer::addDFAState( int index, std::optional<int> terminalIndex, std::vector< DFAState::Edge > edges )
{
  mDFATable[index] = DFAState{ std::move( terminalIndex ), std::move( edges ) };
}

void ParserServer::addLALRState( int index, std::vector< LALRState::Action > actions )
{
  mLALRTable[index] = LALRState{ std::move( actions ) };
}

void ParserServer::addReductor( std::u16string const& name, Reductor reductor )
{
  auto it = std::find_if( mProductionTable.begin(), mProductionTable.end(), [&name]( Production & p )
  {
    return p.getName() == name;
  } );

  if ( it == mProductionTable.end() )
    throw InvalidReductorException{ boost::locale::conv::from_utf( boost::locale::conv::utf_to_utf<wchar_t>( name ), "UTF-8" ) };

  it->setReductor( std::move( reductor ) );
}

void ParserServer::addReductor( std::wstring const& name, Reductor reductor )
{
  return addReductor( boost::locale::conv::utf_to_utf<char16_t>( name ), std::move( reductor ) );
}

bool ParserServer::validateReductors( std::u16string & invalidList )
{
  if ( mReductorsValidated )
    return true;

  for ( auto const& p : mProductionTable )
  {
    if ( !p.getReductor() )
    {
      invalidList += p.getName();
      invalidList += '\n';
    }
  };

  mReductorsValidated = invalidList.empty();
  return mReductorsValidated;
}

void ParserServer::emitStaticCode( std::string const& className, std::ostream & out ) const
{

  out << "#pragma once\n";
  out << "struct " << className << "\n";
  out << "{\n";
  emitCharacterSetTable( out );
  emitSymbolTable( out );
  emitGroupTable( out );
  emitProductionTable( out );
  emitDFATable( out );
  emitLALRTable( out );
  out << "  static constexpr int const initialDFAState = " << mDFAInitialState << ";\n";
  out << "  static constexpr int const initialLALRState = " << mLALRInitialState << ";\n";
  out << "};\n";
}

void ParserServer::emitCharacterSetTable( std::ostream & out ) const
{
  size_t cstSize{};
  std::vector<std::pair<size_t, size_t>> ranges;
  for ( auto const& cst : mCharacterSetTable )
  {
    std::pair<size_t, size_t> r{};
    r.first = cstSize;
    cstSize += cst.size();
    r.second = cstSize;
    ranges.push_back( r );
  }

  out << "  struct CharacterSetRangeList\n";
  out << "  {\n";
  out << "    struct Range\n";
  out << "    {\n";
  out << "      char16_t begin;\n";
  out << "      char16_t end;\n";
  out << "    };\n";
  out << "    Range const* begin;\n";
  out << "    Range const* end;\n";
  out << "    static constexpr Range rs[]\n";
  out << "    {\n";
  for ( size_t i = 0; i < mCharacterSetTable.size(); ++i )
  {
    std::stringstream ss;
    for ( size_t j = 0; j < mCharacterSetTable[i].mRanges.size(); ++j )
    {
      ss << "Range{ ";
      ss << "u'\\u" << std::setfill( '0' ) << std::setw( 4 ) << std::hex << mCharacterSetTable[i].mRanges[j].start << "'";
      ss << ", ";
      ss << "u'\\u" << std::setw( 4 ) << std::hex << mCharacterSetTable[i].mRanges[j].end << "'";
      ss << " }";
      if ( j < mCharacterSetTable[i].mRanges.size() - 1 )
        ss << ", ";
    }
    out << "      " << ss.str();
    if ( i < mCharacterSetTable.size() - 1 )
      out << ",";
    out << "\n";
  }

  out << "    };\n";
  out << "  };\n";
  out << "  static constexpr std::array<CharacterSetRangeList," << mCharacterSetTable.size() << "> characterSetTable\n";
  out << "  {\n";

  for ( size_t i = 0; i < ranges.size(); ++i )
  {
    out << "    CharacterSetRangeList{ CharacterSetRangeList::rs + " << ranges[i].first << ", CharacterSetRangeList::rs + " << ranges[i].second << " }";
    if ( i < ranges.size() - 1 )
      out << ",";
    out << "\n";
  }
  out << "  };\n";
}

void ParserServer::emitSymbolTable( std::ostream & out ) const
{
  out << "  struct SymbolTable\n";
  out << "  {\n";
  out << "    char16_t const* name;\n";
  out << "    enum Type\n";
  out << "    {\n";
  out << "      Nonterminal = 0,\n";
  out << "      Content = 1,\n";
  out << "      Noise = 2,\n";
  out << "      End = 3,\n";
  out << "      GroupStart = 4,\n";
  out << "      GroupEnd = 5,\n";
  out << "      Error = 7\n";
  out << "    } type;\n";
  out << "    size_t idx;\n";
  out << "    std::optional<size_t> groupIdx;\n";
  out << "  };\n";
  out << "  static constexpr std::array<SymbolTable," << mSymbolTable.size() << "> symbolTable\n";
  out << "  {\n";
  for ( size_t i = 0; i < mSymbolTable.size(); ++i )
  {
    out << "    SymbolTable{ ";
    out << "u\"";
    for ( auto c : *mSymbolTable[i].mName )
    {
      char c8 = ( char )c;
      if ( ( char16_t )c8 == c )
      {
        if ( isprint( c8 ) )
        {
          out << c8;
        }
      }
      else
      {
        out << "\\u" << std::setw( 4 ) << std::hex << (int)c << "" << std::dec;
      }
    }
    out << "\", ";
    switch ( mSymbolTable[i].mType )
    {
      case Symbol::Type::Nonterminal:
        out << "SymbolTable::Nonterminal, ";
        break;
      case Symbol::Type::Content:
        out << "SymbolTable::Content, ";
        break;
      case Symbol::Type::Noise:
        out << "SymbolTable::Noise, ";
        break;
      case Symbol::Type::End:
        out << "SymbolTable::End, ";
        break;
      case Symbol::Type::GroupStart:
        out << "SymbolTable::GroupStart, ";
        break;
      case Symbol::Type::GroupEnd:
        out << "SymbolTable::GroupEnd, ";
        break;
      case Symbol::Type::Error:
        out << "SymbolTable::Error, ";
        break;
      default:
        assert( false );
        break;
    }
    out << mSymbolTable[i].mIdx << ", {";
    if ( mSymbolTable[i].mGroupIdx )
    {
      out << *mSymbolTable[i].mGroupIdx;
    }
    out << "} }";
    if ( i < mSymbolTable.size() - 1 )
      out << ",";
    out << "\n";
  }
  out << "  };\n";
}

void ParserServer::emitGroupTable( std::ostream & out ) const
{
  if ( !mGroupTable.empty() )
    throw GE5::Exception{ "emitGroupTable" };
}

void ParserServer::emitProductionTable( std::ostream & out ) const
{

  std::vector<std::pair<size_t, size_t>> ranges{};
  std::vector<int> sit{};
  for ( auto const& p : mProductionTable )
  {
    std::pair<size_t, size_t> r{};
    r.first = sit.size();
    std::copy( p.mSymbolIndices.cbegin(), p.mSymbolIndices.cend(), std::back_inserter( sit ) );
    r.second = sit.size();
    ranges.push_back( r );
  }

  out << "  struct Production\n";
  out << "  {\n";
  out << "    int nonterminal;\n";
  out << "    int const* begin;\n";
  out << "    int const* end;\n";
  out << "    static constexpr int si[]\n";
  out << "    {\n";
  for ( size_t i = 0; i < mProductionTable.size(); ++i )
  {
    std::stringstream ss;
    for ( size_t j = 0; j < mProductionTable[i].mSymbolIndices.size(); ++j )
    {
      ss << mProductionTable[i].mSymbolIndices[j];
      if ( j < mProductionTable[i].mSymbolIndices.size() - 1 )
        ss << ", ";
    }
    out << "      " << ss.str();
    if ( i < mProductionTable.size() - 1 )
      out << ",";
    out << "\n";
  }
  out << "    };\n";
  out << "  };\n";
  out << "  static constexpr std::array<Production," << mProductionTable.size() << "> productionTable\n";
  out << "  {\n";
  for ( size_t i = 0; i < mProductionTable.size(); ++i )
  {
    out << "    Production{ ";
    out << mProductionTable[i].mNonterminal << ", ";
    out << "Production::si + " << ranges[i].first << ", Production::si + " << ranges[i].second;
    out << "}";
    if ( i < mProductionTable.size() - 1 )
      out << ",";
    out << " // " << boost::locale::conv::from_utf( boost::locale::conv::utf_to_utf<wchar_t>( mProductionTable[i].getName() ), "UTF-8" ) << "\n";
  }
  out << "  };\n";
}

void ParserServer::emitDFATable( std::ostream & out ) const
{
  out << "  struct DFAState\n";
  out << "  {\n";
  out << "    struct Edge\n";
  out << "    {\n";
  out << "      int charSetIndex;\n";
  out << "      int targetIndex;\n";
  out << "    };\n";
  out << "    Edge const* begin;\n";
  out << "    Edge const* end;\n";
  out << "    std::optional<int> terminalIndex;\n";

  std::vector<std::pair<size_t, size_t>> ranges{};
  std::vector<DFAState::Edge> et{};
  for ( auto const& dfa : mDFATable )
  {
    std::pair<size_t, size_t> r{};
    r.first = et.size();
    std::copy( dfa.mEdges.cbegin(), dfa.mEdges.cend(), std::back_inserter( et ) );
    r.second = et.size();
    ranges.push_back( r );
  }
  out << "    static constexpr Edge edges[]\n";
  out << "    {\n";
  bool next{};
  for ( size_t i = 0; i < mDFATable.size(); ++i )
  {
    if ( !mDFATable[i].mEdges.empty() )
    {
      std::stringstream ss;
      for ( size_t j = 0; j < mDFATable[i].mEdges.size(); ++j )
      {
        ss << "Edge{ " << mDFATable[i].mEdges[j].charSetIndex << ", " << mDFATable[i].mEdges[j].targetIndex << " }";
        if ( j < mDFATable[i].mEdges.size() - 1 )
          ss << ", ";
      }
      if ( next )
      {
        out << ",\n";
        next = false;
      }
      out << "      " << ss.str();
    }
    if ( i < mDFATable.size() - 1 )
      next = true;
  }
  out << "\n    };\n";
  out << "  };\n";
  out << "  static constexpr std::array<DFAState," << mDFATable.size() << "> dfaTable\n";
  out << "  {\n";
  for ( size_t i = 0; i < mDFATable.size(); ++i )
  {
    if ( ranges[i].first != ranges[i].second )
    {
      out << "    DFAState{ DFAState::edges + " << ranges[i].first << ", DFAState::edges + " << ranges[i].second << ", {";
    }
    else
    {
      out << "    DFAState{ nullptr, nullptr, {";
    }
    if ( mDFATable[i].mTerminalIndex )
      out << " " << *mDFATable[i].mTerminalIndex << " ";
    out << "} }";
    if ( i < mDFATable.size() - 1 )
      out << ",";
    out << "\n";
  }
  out << "  };\n";
}

void ParserServer::emitLALRTable( std::ostream & out ) const
{
  out << "  struct LALRState\n";
  out << "  {\n";
  out << "    struct Action\n";
  out << "    {\n";
  out << "      enum Type\n";
  out << "      {\n";
  out << "        Shift = 1,\n";
  out << "        Reduce = 2,\n";
  out << "        Goto = 3,\n";
  out << "        Accept = 4\n";
  out << "      } type;\n";
  out << "      int symbolIndex;\n";
  out << "      int targetIndex;\n";
  out << "    };\n";
  out << "    Action const* begin;\n";
  out << "    Action const* end;\n";

  std::vector<std::pair<size_t, size_t>> ranges{};
  std::vector<LALRState::Action> as{};
  for ( auto const& lalr : mLALRTable )
  {
    std::pair<size_t, size_t> r{};
    r.first = as.size();
    std::copy( lalr.mAction.cbegin(), lalr.mAction.cend(), std::back_inserter( as ) );
    r.second = as.size();
    ranges.push_back( r );
  }
  out << "    static constexpr Action as[]\n";
  out << "    {\n";
  for ( size_t i = 0; i < mLALRTable.size(); ++i )
  {
    if ( !mLALRTable[i].mAction.empty() )
    {
      for ( size_t j = 0; j < mLALRTable[i].mAction.size(); ++j )
      {
        out << "      Action{ ";
        switch ( mLALRTable[i].mAction[j].action )
        {
          case LALRState::Action::Shift:
            out << "Action::Shift, ";
            break;
          case LALRState::Action::Reduce:
            out << "Action::Reduce, ";
            break;
          case LALRState::Action::Goto:
            out << "Action::Goto, ";
            break;
          case LALRState::Action::Accept:
            out << "Action::Accept, ";
            break;
          default:
            assert( false );
        }
        out << mLALRTable[i].mAction[j].symbolIndex << ", ";
        out << mLALRTable[i].mAction[j].targetIndex << " }";
        if ( j < mLALRTable[i].mAction.size() - 1 || i < mLALRTable.size() - 1 )
          out << ",";
        out << "\n";
      }
    }
  }
  out << "    };\n";
  out << "  };\n";
  out << "  static constexpr std::array<LALRState," << mLALRTable.size() << "> lalrTable\n";
  out << "  {\n";
  for ( size_t i = 0; i < mLALRTable.size(); ++i )
  {
    if ( ranges[i].first != ranges[i].second )
    {
      out << "    LALRState{ LALRState::as + " << ranges[i].first << ", LALRState::as + " << ranges[i].second << " }";
    }
    else
    {
      out << "    LALRState{ nullptr, nullptr }";
    }
    if ( i < mLALRTable.size() - 1 )
      out << ",";
    out << "\n";
  }
  out << "  };\n";
}

}



