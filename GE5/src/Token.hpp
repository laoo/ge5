#ifndef GE5_TOKEN_HPP
#define GE5_TOKEN_HPP

#include "Symbol.hpp"
#include "Position.hpp"

namespace GE5
{

class Token : public Symbol
{
public:
  Token( Symbol const& aSymbol, Position begin, boost::any aValue, int state = -1 );

  Token( Token const& other ) = default;
  Token( Token && other ) = default;
  Token & operator=( Token const& other ) = default;
  Token & operator=( Token && other ) = default;

  // Constructor used during shifting
  Token( Token && aSource, int state );

  boost::any const& value() const;
  boost::any & value();

  Position const& pos() const;
  Position & pos();

  int state() const;

  /**
   * @param limit==0 - no limit, limit>0 - add only limit characters
   * @return number of appended characters
   */
  size_t append( Token const& other, size_t limit = 0 );

private:
  Position mPos;
  boost::any mValue;
  int mState;

};

}

#endif //GE5_SYMBOL_HPP
