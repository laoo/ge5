#ifndef GE5_PARSERSERVER_HPP
#define GE5_PARSERSERVER_HPP

#include "IParserServer.hpp"
#include "Visitor.hpp"
#include "Production.hpp"
#include <ostream>

namespace GE5
{

class ParserServer : public Visitor, public IParserServer, public std::enable_shared_from_this<ParserServer>
{
public:
  ~ParserServer() override = default;

  std::unique_ptr<IParser> open( char const* aBegin, char const* aEnd, int id, bool rememberText ) override;
  std::unique_ptr<IParser> open( char16_t const* aBegin, char16_t const* aEnd, int id, bool rememberText ) override;
  std::unique_ptr<IParser> open( std::string text, int id, bool rememberText ) override;
  std::unique_ptr<IParser> open( std::u16string text, int id, bool rememberText ) override;
  std::unique_ptr<IParser> open( std::wstring source, std::istream & istrm, int id, bool rememberText ) override;

  void addReductor( std::u16string const& name, Reductor reductor ) override;
  void addReductor( std::wstring const& name, Reductor reductor ) override;

  bool validateReductors( std::u16string & invalidList ) override;

  void addProperty( int index, std::u16string name, std::u16string value ) override;
  void setTableCounts( int symbolTable, int characterSetTable, int ruleTable, int dfaTable, int lalrTable, int groupTable ) override;
  void setInitialState( int dfa, int lalr ) override;
  void setCharacterSetTable( int index, int unicodePlane, std::vector< CharacterSetTable::CharacterSetRange > ranges ) override;
  void addSymbol( int index, std::u16string name, Symbol::Type type ) override;
  void addGroup( int index, std::u16string name, int containerIndex, int startIndex, int endIndex, Group::AdvanceMode advanceMode, Group::EndingMode endingMode, std::vector<int> nesting ) override;
  void addProduction( int index, int nonterminal, std::vector<int> symbolIndices ) override;
  void addDFAState( int index, std::optional<int> terminalIndex, std::vector< DFAState::Edge > edges ) override;
  void addLALRState( int index, std::vector< LALRState::Action > actions ) override;

  ParserServer( char const* aBegin, char const* aEnd );

  void emitStaticCode( std::string const& className, std::ostream & out ) const override;

private:
  char const* readHeader( char const* aHeader ) const;

  void emitCharacterSetTable( std::ostream & out ) const;
  void emitSymbolTable( std::ostream & out ) const;
  void emitGroupTable( std::ostream & out ) const;
  void emitProductionTable( std::ostream & out ) const;
  void emitDFATable( std::ostream & out ) const;
  void emitLALRTable( std::ostream & out ) const;
private:
  friend class ParserData;

  int mDFAInitialState;
  int mLALRInitialState;

  std::vector<CharacterSetTable> mCharacterSetTable;
  std::vector<Symbol> mSymbolTable;
  std::vector<Group> mGroupTable;
  std::vector<Production> mProductionTable;
  std::vector<DFAState> mDFATable;
  std::vector<LALRState> mLALRTable;
  std::map<std::u16string, std::u16string> mProperties;

  bool mReductorsValidated;

};
}

#endif //GE5_PARSERSERVER_HPP
