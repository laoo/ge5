#include "stdafx.h"
#include "Exception.hpp"
#include "Record.hpp"
#include "Visitor.hpp"

namespace GE5
{

Record::Record( char const* begin, int numberOfEntries ) : mIt{ begin }, mNumberOfEntries{ numberOfEntries }
{
}

bool Record::visit( Visitor & aVisitor )
{
  if ( mIt == nullptr )
    return false;

  switch ( char c = getByte() )
  {
    case 'p': // Property
    {
      int index = getInteger();
      std::u16string name = getString();
      std::u16string value = getString();
      aVisitor.addProperty( index, std::move( name ), std::move( value ) );
    }
    break;
    case 't': // Table Counts
    {
      int symbolTable = getInteger();
      int characterSetTable = getInteger();
      int ruleTable = getInteger();
      int dfaTable = getInteger();
      int lalrTable = getInteger();
      int groupTable = getInteger();
      aVisitor.setTableCounts( symbolTable, characterSetTable, ruleTable, dfaTable, lalrTable, groupTable );
    }
    break;
    case 'c': // Characted Set Table
    {
      int index = getInteger();
      int unicodePlain = getInteger();
      int rangeCount = getInteger();
      getEmpty();

      std::vector< CharacterSetTable::CharacterSetRange > range;
      range.reserve( rangeCount );
      for ( int i = 0; i < rangeCount; ++i )
      {
        int startChar = getInteger();
        int endChar = getInteger();
        range.emplace_back( CharacterSetTable::CharacterSetRange{ ( char16_t )startChar, ( char16_t )endChar } );
      }
      aVisitor.setCharacterSetTable( index, unicodePlain, std::move( range ) );
    }
    break;
    case 'S': // Symbol
    {
      int index = getInteger();
      std::u16string name = getString();
      int type = getInteger();
      aVisitor.addSymbol( index, std::move( name ), ( Symbol::Type )type );
    }
    break;
    case 'g': // Group
    {
      int index = getInteger();
      std::u16string name = getString();
      int containerIndex = getInteger();
      int startIndex = getInteger();
      int endIndex = getInteger();
      int advanceMode = getInteger();
      int endingMode = getInteger();
      getEmpty();
      int nestingCount = getInteger();
      std::vector<int> nesting;
      nesting.reserve( nestingCount );
      for ( int i = 0; i < nestingCount; ++i )
      {
        nesting.push_back( getInteger() );
      }
      aVisitor.addGroup( index, name, containerIndex, startIndex, endIndex,
        ( Group::AdvanceMode )advanceMode, ( Group::EndingMode )endingMode, std::move( nesting ) );
    }
    break;
    case 'R': // Production
    {
      int index = getInteger();
      int nonterminal = getInteger();
      getEmpty();
      std::vector<int> symbolIndices;
      symbolIndices.reserve( mNumberOfEntries );
      while ( mNumberOfEntries > 0 )
      {
        symbolIndices.push_back( getInteger() );
      }
      aVisitor.addProduction( index, nonterminal, std::move( symbolIndices ) );

    }
    break;
    case 'I': // Initial State
    {
      int dfa = getInteger();
      int lalr = getInteger();
      aVisitor.setInitialState( dfa, lalr );
    }
    break;
    case 'D': // DFA state
    {
      int index = getInteger();
      bool acceptsTerminal = getBoolean();
      int acceptedTerminalIndex = getInteger();
      getEmpty();
      std::vector< DFAState::Edge > edges;
      if ( mNumberOfEntries % 3 != 0 )
        EGTException{ "Bad number of edges" };
      edges.reserve( mNumberOfEntries / 3 );
      while ( mNumberOfEntries > 0 )
      {
        int charSetIndex = getInteger();
        int targetIndex = getInteger();
        getEmpty();
        edges.emplace_back( DFAState::Edge( charSetIndex, targetIndex ) );
      }
      std::optional<int> ati;
      if ( acceptsTerminal )
        ati = acceptedTerminalIndex;
      aVisitor.addDFAState( index, ati, std::move( edges ) );
    }
    break;
    case 'L': // LALR state
    {
      int index = getInteger();
      getEmpty();
      std::vector< LALRState::Action > actions;
      if ( mNumberOfEntries % 4 != 0 )
        EGTException{ "Bad number of actions" };
      actions.reserve( mNumberOfEntries / 4 );
      while ( mNumberOfEntries > 0 )
      {
        int symbolIndex = getInteger();
        int action = getInteger();
        int targetIndex = getInteger();
        getEmpty();
        actions.emplace_back( LALRState::Action{ symbolIndex, ( LALRState::Action::Type )action, targetIndex } );
      }
      aVisitor.addLALRState( index, std::move( actions ) );
    }
    break;
    default:
      throw EGTException( "unknow record type" );
  }

  if ( mNumberOfEntries > 0 )
    throw EGTException( "Record not exhausted" );

  return true;
}

void Record::getEmpty()
{
  entryGuard( 'E' );
  mNumberOfEntries -= 1;
}

unsigned char Record::getByte()
{
  entryGuard( 'b' );
  mNumberOfEntries -= 1;

  return static_cast< unsigned char >( *mIt++ );
}

bool Record::getBoolean()
{
  entryGuard( 'B' );
  mNumberOfEntries -= 1;

  return *mIt++ == 1;
}

int Record::getInteger()
{
  entryGuard( 'I' );
  mNumberOfEntries -= 1;

  unsigned char lo = static_cast< unsigned char >( *mIt++ );
  unsigned char hi = static_cast< unsigned char >( *mIt++ );

  return lo + ( hi << 8 );
}

std::u16string Record::getString()
{
  entryGuard( 'S' );
  mNumberOfEntries -= 1;

  std::u16string ret( reinterpret_cast< char16_t const* >( mIt ) );
  mIt += ( ret.size() + 1 ) * sizeof( char16_t );
  return ret;
}

void Record::entryGuard( char entryCode )
{
  if ( mNumberOfEntries <= 0 )
    throw EGTException( "Out of entries" );
  if ( *mIt++ != entryCode )
    throw EGTException( "Bad entry" );
}

}
