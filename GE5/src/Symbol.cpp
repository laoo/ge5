#include "stdafx.h"
#include "Symbol.hpp"
#include "Exception.hpp"

namespace GE5
{

Symbol::Symbol() : mName{}, mType{}, mIdx{}, mGroupIdx{}
{
}

Symbol::Symbol( std::u16string name, Type type, size_t idx ) :
  mName{ std::make_shared<std::u16string>( std::move( name ) ) },
  mType{ type },
  mIdx{ idx },
  mGroupIdx{}
{
}

Symbol::Type Symbol::type() const
{
  return mType;
}

std::u16string const& Symbol::name() const
{
  return *mName;
}

size_t Symbol::idx() const
{
  return mIdx;
}

size_t Symbol::groupIdx() const
{
  if ( !mGroupIdx )
    throw EGTException{ "No group in symbol" };
  return *mGroupIdx;
}

void Symbol::setGroup( size_t idx )
{
  mGroupIdx = idx;
}

}

