#ifndef GE5_PARSERPDATA_HPP
#define GE5_PARSERPDATA_HPP

#include "ParserServer.hpp"

namespace GE5
{

class ParserData
{
protected:
  ParserData( std::shared_ptr<ParserServer> aServer ) :
    mServer{ std::move( aServer ) },
    mCharacterSetTable{ mServer->mCharacterSetTable },
    mSymbolTable{ mServer->mSymbolTable },
    mGroupTable{ mServer->mGroupTable },
    mProductionTable{ mServer->mProductionTable },
    mDFATable{ mServer->mDFATable },
    mLALRTable{ mServer->mLALRTable },
    mInitialDFA{ mServer->mDFAInitialState }
  {
  }

  int ParserData::initialLALR() const
  {
    return mServer->mLALRInitialState;
  }

private:
  std::shared_ptr<ParserServer> mServer;

protected:
  std::vector<CharacterSetTable> const& mCharacterSetTable;
  std::vector<Symbol> const& mSymbolTable;
  std::vector<Group> const& mGroupTable;
  std::vector<Production> const& mProductionTable;
  std::vector<DFAState> const& mDFATable;
  std::vector<LALRState> const& mLALRTable;
  int const mInitialDFA;
};

}

#endif //GE5_PARSERPDATA_HPP
