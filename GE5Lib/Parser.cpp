#include "Parser.hpp"
#include "Exception.hpp"
#include "Lexer.hpp"
#include <cassert>

namespace ge5
{
namespace
{

LALRState::Action const* action( LALRState const & state, int symbolIdx )
{
  auto it = std::find_if( state.begin, state.end, [symbolIdx]( LALRState::Action const& a )
  {
    return a.symbolIndex == symbolIdx;
  } );

  if ( it != state.end )
  {
    return &*it;
  }
  else
  {
    return nullptr;
  }
}

}


Parser::Parser( ParserData const & data ) : mData{ data }
{
}

Parser::Ctx Parser::parseLALR( Ctx ctx, Lexeme const& lexeme ) const
{
  LALRState::Action const* pAction = action( mData.lalrTable[ctx.currentLALR], lexeme.symbol->idx );

  if ( !pAction )
  {
    throw ParseError{ lexeme };
  }

  switch ( pAction->type )
  {
    case LALRState::Action::Shift:
    {
      std::unique_ptr<Token> token{ new Token{ lexeme, ctx.currentLALR = pAction->targetIndex, std::move( ctx.stack ), {} } };
      ctx.stack = std::move( token );
      ctx.action = Ctx::Action::Shift;
      return ctx;
    }
    case LALRState::Action::Reduce:
    {
      Production const& p = mData.productionTable[pAction->targetIndex];
      int newOff = ctx.stack->lexeme.offset;
      std::unique_ptr<ge5::IParseTreeNode> node;
      auto const* back = &ctx.stack->lexeme.content.back() + 1;
      std::tie( ctx.stack, node ) = mData.reductor( std::move( ctx.stack ), pAction->targetIndex );
      auto const* front = &ctx.stack->lexeme.content.front();
      ctx.stack = std::move( ctx.stack->next );
      int state = ctx.stack ? ctx.stack->state : mData.initialLALRState;
      pAction = action( mData.lalrTable[state], p.nonterminal );
      if ( !pAction )
      {
        throw ParseError{ lexeme };
      }
      Lexeme l{ &mData.symbolTable[p.nonterminal], std::u16string_view{ front, (size_t)( back - front ) }, newOff };
      std::unique_ptr<Token> token{ new Token{ l, ctx.currentLALR = pAction->targetIndex, std::move( ctx.stack ), std::move( node ) } };
      ctx.stack = std::move( token );
      ctx.action = Ctx::Action::Reduce;
      return ctx;
    }
    case LALRState::Action::Accept:
      ctx.action = Ctx::Action::Accept;
      return ctx;
    default:
      throw ParseError{ lexeme };
  }
}


std::unique_ptr<ge5::IParseTreeNode> Parser::parse( std::u16string_view text ) const
{
  Lexer lex{ mData.initialDFAState, text };

  Ctx ctx{ Ctx::Action::None, {}, mData.initialLALRState };

  int currentLALR{ mData.initialLALRState };

  for ( ;; )
  {
    Lexeme l = lex.produceLexeme( mData );
    assert( l.symbol );
    while ( l.symbol->type == Symbol::Noise )
    {
      l = lex.produceLexeme( mData );
      assert( l.symbol );
    }

    do
    {
      ctx = parseLALR( std::move( ctx ), l );
    } while ( ctx.action == Ctx::Action::Reduce );

    if ( ctx.action == Ctx::Action::Accept )
      return std::move( ctx.stack->node );
  }
}
}
