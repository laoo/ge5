#pragma once
#include <exception>
#include "StructDecl.hpp"

namespace ge5
{

class SyntaxError : public std::exception
{
public:
  explicit SyntaxError( int offset ) : exception{}, mOffset{ offset }
  {
  }
  ~SyntaxError() override = default;

  int offset() const
  {
    return mOffset;
  }

private:
  int mOffset;
};

class ParseError : public std::exception
{
public:
  explicit ParseError( Lexeme lexeme ) : exception{}, mLexeme{ lexeme }
  {
  }
  ~ParseError() override = default;

  Lexeme const& lexeme() const
  {
    return mLexeme;
  }

private:
  Lexeme mLexeme;
};

} //namespace ge5
