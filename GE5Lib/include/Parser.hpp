#pragma once

#include "StructDecl.hpp"

namespace ge5
{

class Parser
{
public:
  Parser( ParserData const& data );

  std::unique_ptr<ge5::IParseTreeNode> parse( std::u16string_view text ) const;

private:

  struct Ctx
  {
    enum class Action
    {
      None,
      Accept,
      Shift,
      Reduce
    } action;
    std::unique_ptr<Token> stack;
    int currentLALR;
  };


  Ctx parseLALR( Ctx ctx, Lexeme const& lexeme ) const;

  ParserData const& mData;

};

}

