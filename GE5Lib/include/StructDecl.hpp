#pragma once

#include <optional>
#include <functional>
#include <gsl/span>
#include <string_view>

namespace ge5
{
struct CharacterSetRangeList
{
  struct Range
  {
    char16_t begin;
    char16_t end;
  };
  Range const* begin;
  Range const* end;
};

struct Symbol
{
  char16_t const* name;
  enum Type
  {
    Nonterminal = 0,
    Content = 1,
    Noise = 2,
    End = 3,
    GroupStart = 4,
    GroupEnd = 5,
    Error = 7
  } type;
  size_t idx;
  std::optional<size_t> groupIdx;
};

struct DFAState
{
  struct Edge
  {
    int charSetIndex;
    int targetIndex;
  };
  Edge const* begin;
  Edge const* end;
  std::optional<int> terminalIndex;
};

struct Group
{
public:

  enum AdvanceMode
  {
    Token = 0,
    Character = 1
  };

  enum EndingMode
  {
    Open = 0,
    Closed = 1
  };

  char16_t const* name;
  int containerIndex;
  int startIndex;
  int endIndex;
  AdvanceMode advanceMode;
  EndingMode endingMode;
};

struct Production
{
  int nonterminal;
};

struct LALRState
{
  struct Action
  {
    enum Type
    {
      Shift = 1,
      Reduce = 2,
      Goto = 3,
      Accept = 4
    } type;
    int symbolIndex;
    int targetIndex;
  };
  Action const* begin;
  Action const* end;
};

struct IParseTreeNode
{
  virtual ~IParseTreeNode() = default;
};

struct Lexeme
{
  Symbol const* symbol;
  std::u16string_view content;
  int offset;
};


struct Token
{
  Lexeme lexeme;
  int state;
  std::unique_ptr<Token> next;
  std::unique_ptr<IParseTreeNode> node;
};

struct ParserData
{
  gsl::span<CharacterSetRangeList const> characterSetTable;
  gsl::span<Symbol const> symbolTable;
  gsl::span<Group const> groupTable;
  gsl::span<Production const> productionTable;
  gsl::span<DFAState const> dfaTable;
  gsl::span<LALRState const> lalrTable;
  int const initialDFAState;
  int const initialLALRState;
  //returns pointer to last token of the reduction and reduced node
  std::function<std::pair<std::unique_ptr<ge5::Token>,std::unique_ptr<ge5::IParseTreeNode>>( std::unique_ptr<ge5::Token> token, size_t reduction )> reductor;
};

}
