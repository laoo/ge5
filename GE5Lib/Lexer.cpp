#include "Lexer.hpp"
#include "Exception.hpp"

#include <cassert>

namespace ge5
{
namespace
{
Symbol const* findKind( ParserData const & data, Symbol::Type type )
{
  auto it = std::find_if( data.symbolTable.cbegin(), data.symbolTable.cend(), [type]( Symbol const& s )
  {
    return s.type == type;
  } );

  if ( it == data.symbolTable.cend() )
    return nullptr;
  else
    return &*it;
}

bool contains( CharacterSetRangeList const& list, char16_t c )
{
  for ( auto const* range = list.begin; range != list.end; ++range )
  {
    if ( c >= range->begin && c <= range->end )
      return true;
  }

  return false;
}

}

Lexer::Lexer( int const initialDFA, std::u16string_view text ) :
  mInitialDFA{ initialDFA },
  mText{ text },
  mOffset{},
  mGroupStack{}
{
}

int Lexer::lookahead( size_t aIdx )
{
  if ( mOffset + aIdx >= mText.size() )
    return -1;
  else
    return mText[mOffset + aIdx];
}

std::u16string_view Lexer::lookaheadString( size_t size ) const
{
  assert( mOffset + size < mText.size() );

  return std::u16string_view{ &mText[mOffset], size + 1 };
}

Lexeme Lexer::lookaheadDFA( ParserData const & data )
{
  int currentDFA = mInitialDFA;
  int target = -1;
  int lastAcceptState = -1;
  size_t lastAcceptPosition = 0;

  int c = lookahead( 0 );

  if ( c < 0 )
    return Lexeme{ findKind( data, Symbol::End ), {}, mOffset };

  for ( size_t currentPosition = 0; ; ++currentPosition, c = lookahead( currentPosition ) )
  {
    if ( c >= 0 )
    {
      char16_t ch = ( char16_t )c;
      auto dfa = std::find_if( data.dfaTable[currentDFA].begin, data.dfaTable[currentDFA].end, [&]( DFAState::Edge const& e )
      {
        return contains( data.characterSetTable[e.charSetIndex], ch );
      } );

      if ( dfa != data.dfaTable[currentDFA].end )
      {
        currentDFA = dfa->targetIndex;
        if ( data.dfaTable[currentDFA].terminalIndex )
        {
          lastAcceptState = currentDFA;
          lastAcceptPosition = currentPosition;
        }
        continue;
      }
    }

    if ( lastAcceptState == -1 )
    {
      throw SyntaxError{ mOffset };
    }
    else
    {
      return Lexeme{ &data.symbolTable[*data.dfaTable[lastAcceptState].terminalIndex], lookaheadString( lastAcceptPosition ), mOffset };
    }
  }
}

void Lexer::comsumeBuffer( size_t aCnt )
{
  mOffset += aCnt;
}


Lexeme Lexer::produceLexeme( ParserData const & data )
{
  for ( ;; )
  {
    Lexeme t = lookaheadDFA( data );

    bool nestGroup = false;
    if ( t.symbol->type == Symbol::GroupStart )
    {
      if ( mGroupStack.empty() )
        nestGroup = true;
      else
        nestGroup = false;
    }

    if ( nestGroup )
    {
      comsumeBuffer( t.content.size() );
      mGroupStack.push_back( t );
    }
    else if ( mGroupStack.empty() )
    {
      //The token is ready to be analyzed.
      comsumeBuffer( t.content.size() );
      return t;
    }
    else if ( mGroupStack.back().symbol->groupIdx && data.groupTable[*mGroupStack.back().symbol->groupIdx].endIndex == t.symbol->idx )
    {
      //End the current group
      Lexeme pop = std::move( mGroupStack.back() );
      mGroupStack.pop_back();

      if ( data.groupTable[*pop.symbol->groupIdx].endingMode == Group::Closed )
      {
        pop.content = { pop.content.data(), pop.content.size() + t.content.size() };
        comsumeBuffer( pop.content.size() );
      }

      if ( mGroupStack.empty() )
      {
        return Lexeme{ &data.symbolTable[data.groupTable[*pop.symbol->groupIdx].containerIndex], pop.content, mOffset };
      }
      else
      {
        mGroupStack.back().content = { mGroupStack.back().content.data(), mGroupStack.back().content.size() + pop.content.size() };
      }
    }
    else if ( t.symbol->type == Symbol::End )
    {
      //EOF always stops the loop. The caller function (Parse) can flag a runaway group error.
      t.offset = mOffset;
      return t;
    }
    else
    {
      //We are in a group, Append to the Token on the top of the stack.
      //Take into account the Token group mode  
      Lexeme & top = mGroupStack.back();

      assert( top.symbol->groupIdx );

      if ( data.groupTable[*top.symbol->groupIdx].advanceMode == Group::Token )
      {
        top.content = { top.content.data(), top.content.size() + t.content.size() };
        comsumeBuffer( t.content.size() );
      }
      else
      {
        top.content = { top.content.data(), top.content.size() + 1 };
        comsumeBuffer( 1 );
      }
    }
  }
}

}
