#pragma once

#include "StructDecl.hpp"

namespace ge5
{

class Lexer
{
public:
  Lexer( int const initialDFA, std::u16string_view text );

  Lexeme produceLexeme( ParserData const & data );

private:
  Lexeme lookaheadDFA( ParserData const & data );
  int lookahead( size_t aIdx );
  std::u16string_view lookaheadString( size_t size ) const;
  void comsumeBuffer( size_t aCnt );

private:
  int const mInitialDFA;
  std::u16string_view mText;
  int mOffset;
  std::vector<Lexeme> mGroupStack;

};

}