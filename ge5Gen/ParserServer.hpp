#ifndef GE5_PARSERSERVER_HPP
#define GE5_PARSERSERVER_HPP

#include "Visitor.hpp"

namespace GE5Gen
{

class ParserServer : public Visitor, public std::enable_shared_from_this<ParserServer>
{
public:
  ~ParserServer() override = default;

  static std::shared_ptr<ParserServer> create( char const* aBegin, char const* aEnd );
  static std::shared_ptr<ParserServer> create( std::vector<char> const& tables );
  static std::shared_ptr<ParserServer> create( std::filesystem::path const& aTableFileName );
  static std::shared_ptr<ParserServer> create( std::ifstream & fin );


  void addProperty( int index, std::u16string name, std::u16string value ) override;
  void setTableCounts( int symbolTable, int characterSetTable, int ruleTable, int dfaTable, int lalrTable, int groupTable ) override;
  void setInitialState( int dfa, int lalr ) override;
  void setCharacterSetTable( int index, int unicodePlane, std::vector< CharacterSetTable::CharacterSetRange > ranges ) override;
  void addSymbol( int index, std::u16string name, Symbol::Type type ) override;
  void addGroup( int index, std::u16string name, int containerIndex, int startIndex, int endIndex, Group::AdvanceMode advanceMode, Group::EndingMode endingMode, std::vector<int> nesting ) override;
  void addProduction( int index, int nonterminal, std::vector<int> symbolIndices ) override;
  void addDFAState( int index, std::optional<int> terminalIndex, std::vector< DFAState::Edge > edges ) override;
  void addLALRState( int index, std::vector< LALRState::Action > actions ) override;

  ParserServer( char const* aBegin, char const* aEnd );

  void emitStaticCode( std::string const& nsName, std::ostream & out ) const;

private:
  char const* readHeader( char const* aHeader ) const;

  void emitCharacterSetTable( std::ostream & out ) const;
  void emitSymbolTable( std::ostream & out ) const;
  void emitGroupTable( std::ostream & out ) const;
  void emitProductionTable( std::ostream & out ) const;
  void emitDFATable( std::ostream & out ) const;
  void emitLALRTable( std::ostream & out ) const;
  void emitReductors( std::ostream & out ) const;
private:
  int mDFAInitialState;
  int mLALRInitialState;

  std::vector<std::unique_ptr<CharacterSetTable>> mCharacterSetTable;
  std::vector<std::unique_ptr<Symbol>> mSymbolTable;
  std::vector<Group> mGroupTable;
  std::vector<Production> mProductionTable;
  std::vector<DFAState> mDFATable;
  std::vector<LALRState> mLALRTable;
  std::map<std::u16string, std::u16string> mProperties;

};

}

#endif //GE5_PARSERSERVER_HPP
