#ifndef GE5_EXCEPTION_HPP
#define GE5_EXCEPTION_HPP

#include <stdexcept>

namespace GE5Gen
{

class Exception : public std::runtime_error
{
public:
  explicit Exception( std::string const& aWhat ) : runtime_error{ aWhat }
  {
  }
  ~Exception() override = default;
};

class EGTException : public Exception
{
public:
  explicit EGTException( std::string const& aWhat ) : Exception{ aWhat }
  {
  }
  ~EGTException() override = default;
};

} //namespace GE5

#endif //GE5_EXCEPTION_HPP
