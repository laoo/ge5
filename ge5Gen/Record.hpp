#ifndef GE5_RECORD_HPP
#define GE5_RECORD_HPP


namespace GE5Gen
{
class Visitor;

class Record
{
public:

  Record( char const* begin = nullptr, int numberOfEntries = 0 );

  bool visit( Visitor & aVisitor );

public:
  void getEmpty();
  unsigned char getByte();
  bool getBoolean();
  int getInteger();
  std::u16string getString();

  void entryGuard( char entryCode );

  char const* mIt;
  int mNumberOfEntries;
};

}

#endif //GE5_RECORD_HPP
