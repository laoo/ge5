#include "stdafx.h"
#include "ParserServer.hpp"
#include "Exception.hpp"

int wmain( int argc, wchar_t* argv[] )
{
  try
  {
    if ( argc != 3 )
    {
      std::cerr << "GE5Gen input.egt output.hpp\n";
      return 1;
    }

    std::filesystem::path finName{ argv[1] };
    if ( !std::filesystem::exists( finName ) )
    {
      std::cerr << finName << " does not exist\n";
      return 1;
    }

    std::vector<char> egt;
    {
      std::ifstream fin{ finName, std::ios::binary };
      egt.resize( (size_t)std::filesystem::file_size( finName ) );
      fin.read( egt.data(), egt.size() );
    }

    if ( egt.empty() )
    {
      std::cerr << "Error reading input file " << finName << std::endl;
      return 1;
    }

    std::shared_ptr<GE5Gen::ParserServer> pParserServer = GE5Gen::ParserServer::create( &*egt.cbegin(), &*egt.cbegin() + egt.size() );

    std::ofstream fout{ argv[2], std::ios::binary };

    pParserServer->emitStaticCode( finName.filename().replace_extension().string(), fout );
  }
  catch ( GE5Gen::Exception const& e )
  {
    std::cerr << e.what();
  }

  return 0;
}
