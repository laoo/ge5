#ifndef GE5_CHARACTERSETTABLE_HPP
#define GE5_CHARACTERSETTABLE_HPP

#include <ostream>

namespace GE5Gen
{

struct CharacterSetTable
{
public:

  struct CharacterSetRange
  {
    CharacterSetRange( char16_t aStart, char16_t aEnd ) : start( aStart ), end( aEnd ) {}

    char16_t start;
    char16_t end;
  };

  std::vector<CharacterSetRange> ranges;
};

}

#endif //GE5_CHARACTERSETTABLE_HPP
