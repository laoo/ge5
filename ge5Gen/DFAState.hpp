#ifndef GE5_DFASTATE_HPP
#define GE5_DFASTATE_HPP

namespace GE5Gen
{

struct DFAState
{
  struct Edge
  {
    Edge( int aCharSetIndex, int aTargetIndex ) : charSetIndex{ aCharSetIndex }, targetIndex{ aTargetIndex } {}
    int charSetIndex;
    int targetIndex;
  };

  std::optional<int> terminalIndex;
  std::vector< DFAState::Edge > edges;
};

}

#endif //GE5_DFASTATE_HPP
