#include "stdafx.h"
#include "Exception.hpp"
#include "EGTReader.hpp"

namespace GE5Gen
{

EGTReader::EGTReader( char const * begin, char const * end ) : mIt{ begin }, mEnd{ end }
{

}

Record EGTReader::read()
{
  if ( mIt < mEnd )
  {
    char c = getC();
    if ( c == 'M' )
    {
      unsigned int numberOfEntries = getUC();
      numberOfEntries += getUC() << 8;
      char const* begin = mIt;
      for ( unsigned int i = 0; i < numberOfEntries; ++i )
      {
        char c = getC();
        switch ( c )
        {
          case 'E':
            break;
          case 'b':
          case 'B':
            mIt += 1;
            break;
          case 'I':
            mIt += 2;
            break;
          case 'S':
          {
            unsigned short s = 0;
            do
            {
              s = getUC();
              s |= getUC() << 8;
            } while ( s != 0 );
          }
          break;
          default:
            throw EGTException{ "Unknown entry" };
        }
      }

      return Record( begin, numberOfEntries );
    }
    else
    {
      throw EGTException{ "Bad Record" };
    }
  }

  return {};

}

char EGTReader::getC()
{
  if ( mIt < mEnd )
    return *mIt++;
  else
    throw EGTException{ "End of File" };
}

unsigned char EGTReader::getUC()
{
  return static_cast< unsigned char >( getC() );
}

}
