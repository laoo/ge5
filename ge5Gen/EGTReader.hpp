#ifndef GE5_EGTREADER_HPP
#define GE5_EGTREADER_HPP

#include "Record.hpp"

namespace GE5Gen
{

class EGTReader
{
public:

  EGTReader( char const * begin, char const * end );

  Record read();

private:
  char getC();
  unsigned char getUC();

  char const * mIt;
  char const * mEnd;
};

}

#endif //GE5_EGTREADER_HPP
